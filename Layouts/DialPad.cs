﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Layouts
{


    [Activity(Label = "DialPad")]
    public class DialPad : Activity
    {
        private TextView numberTyped;
        private StringBuilder value;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.DialPad);
            numberTyped = FindViewById<TextView>(Resource.Id.textTyped);
            value = new StringBuilder("0");


            // Create your application here
        }
        [Java.Interop.Export("ButtonClick")]
        public void ButtonClick(View v)
        {
            Button button = (Button)v;
            if ("0123456789.".Contains(button.Text))
            {
                value = value.Append(button.Text);
                numberTyped.Text = value.ToString();
            }
        }
    }
}
