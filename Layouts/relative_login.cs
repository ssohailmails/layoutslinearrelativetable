﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Layouts
{
    [Activity(Label = "relative_login")]
    public class relative_login : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.relative_login);


            Button button = FindViewById<Button>(Resource.Id.dialPadButton);
            button.Click += delegate {
                StartActivity(typeof(DialPad));
            };

            // Create your application here
        }
    }
}
